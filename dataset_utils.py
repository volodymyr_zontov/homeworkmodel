import os
import csv
import urllib.request
import shutil

CSV_FILE = 'data.csv'
IMAGES_SAVE_FOLDER = 'images_data'


def read_csv(filename):
    data = []
    with open(filename, 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=';', )
        for row in reader:
            data.append(row)
    return data[1:]


def save_file_from_url(url, local_path_to_save):
    filename = os.path.join(local_path_to_save, url.split("/")[-1])
    with urllib.request.urlopen(url) as response:
        with open(filename, 'wb') as out_file:
            data = response.read()
            out_file.write(data)


def get_data():
    data = read_csv(CSV_FILE)
    for url, label in data:
        label_folder = os.path.join(IMAGES_SAVE_FOLDER, label)
        try:
            os.mkdir(label_folder)
        except FileExistsError:
            pass
        try:
            save_file_from_url(url, label_folder)
        except Exception as ex:
            print("Error", url)


def move_20percent_from_folder_to_folder(folder_source, folder_destiny):
    files = [file.replace(".jpg", "") for file in os.listdir(folder_source) if ".jpg" in file]
    files_count = int(len(files) * 0.2)
    files_to_move = list(set(files))[:files_count]
    for file in files_to_move:
        file_jpg = f"{file}.jpg"
        file_xml = f"{file}.xml"
        shutil.move(os.path.join(folder_source, file_jpg), os.path.join(folder_destiny, file_jpg))
        shutil.move(os.path.join(folder_source, file_xml), os.path.join(folder_destiny, file_xml))


if __name__ == "__main__":
    get_data()
