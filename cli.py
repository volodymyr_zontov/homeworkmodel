import argparse
import os
from yolo_utils import create_yolo
from model_tools import ModelTools

parser = argparse.ArgumentParser(description='Required: image filename')
parser.add_argument("filename", help="Filename")
parser.add_argument("draw", help="Draw result on image")
args = parser.parse_args()
filename = args.filename
draw = args.draw
draw = draw not in ["False", "false", "F", "f"]

classes = {0: "mercedes", 1: "volkswagen"}
yolo = create_yolo(len(classes))
yolo.load_weights("yolo_weights/yolov3_custom")

file_out = filename.rsplit("/")[-1]
try:
    os.mkdir("results")
except FileExistsError:
    pass

model = ModelTools(yolo, classes, filename, f"./results/result_{file_out}.jpg")
bbox_detected = model.detector()
class_detected = model.classifier(draw=draw)
print(f"Class detected: {class_detected}, bbox: {bbox_detected}")
