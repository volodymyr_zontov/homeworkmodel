import os
import argparse
from yolo_utils import create_yolo
from model_tools import ModelTools

parser = argparse.ArgumentParser(description='Required: path to folder')
parser.add_argument("path", help="Path to folder")
parser.add_argument("draw", help="Draw result on image")
args = parser.parse_args()
path = args.path
draw = args.draw
draw = draw not in ["False", "false", "F", "f"]

classes = {0: "mercedes", 1: "volkswagen"}
Darknet_weights = "yolo_weights/yolov3.weights"
yolo = create_yolo(len(classes))
yolo.load_weights("yolo_weights/yolov3_custom")

try:
    os.mkdir("results")
except FileExistsError:
    pass

files = sorted([filename for filename in os.listdir(path) if "jpg" in filename])
for file in files:
    model = ModelTools(yolo, classes, os.path.join(path, file), f"./results/{file}_pred.jpg")
    bbox_detected = model.detector()
    class_detected = model.classifier(draw=draw)
    print(f"File: {file}  Class detected: {class_detected}, bbox: {bbox_detected}")
