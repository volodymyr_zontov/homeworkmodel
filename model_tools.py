from yolo_utils import *


class ModelTools:
    def __init__(self, yolo_model, classes, image_path, out_image_path):
        self._yolo_model = yolo_model
        self._classes = classes
        self._out_image_path = out_image_path
        self._image_path = image_path
        self._image = cv2.imread(self._image_path)
        self._input_size = 416
        self._score_threshold = 0.3
        self._iou_threshold = 0.45
        self.bboxes = self.get_bboxes()

    def get_bboxes(self):
        original_image = cv2.cvtColor(self._image, cv2.COLOR_BGR2RGB)
        image_data = image_preprocess(np.copy(original_image), [self._input_size, self._input_size])
        image_data = image_data[np.newaxis, ...].astype(np.float32)

        pred_bbox = self._yolo_model.predict(image_data)
        pred_bbox = [tf.reshape(x, (-1, tf.shape(x)[-1])) for x in pred_bbox]
        pred_bbox = tf.concat(pred_bbox, axis=0)

        bboxes = postprocess_boxes(pred_bbox, original_image, self._input_size, self._score_threshold)
        bboxes = nms(bboxes, self._iou_threshold)
        bboxes.sort(key=lambda bbox: abs(bbox[0] - bbox[2]) * abs(bbox[1] - bbox[3]), reverse=True)
        return bboxes

    def detector(self, draw=False):
        if self.bboxes:
            bbox = self.bboxes[0]
            detected_bbox = tuple(map(int, bbox[:4]))
            if draw:
                self._draw_bbox(detected_bbox)
        else:
            detected_bbox = None
        return detected_bbox

    def classifier(self, draw=False):
        if self.bboxes:
            bbox = self.bboxes[0]
            detected_bbox_class = self._classes[(int(bbox[5]))]
            if draw:
                self._draw_bboxes_with_classes(bbox)
        else:
            detected_bbox_class = None
        return detected_bbox_class

    def _draw_bbox(self, bbox, color=(255, 0, 0)):
        image = self._image.copy()
        image_h, image_w, _ = image.shape
        cv2.rectangle(self._image, (bbox[0], bbox[1]), (bbox[2], bbox[3]), color)
        cv2.imwrite(self._out_image_path, image)

    def _draw_bboxes_with_classes(self, bbox, show_confidence=True):
        image = self._image.copy()
        image_h, image_w, _ = image.shape
        colors = {0: (255, 0, 0), 1: (0, 0, 255)}
        coor = np.array(bbox[:4], dtype=np.int32)
        score = bbox[4]
        class_ind = int(bbox[5])
        bbox_color = colors[class_ind]
        bbox_thick = int(0.6 * (image_h + image_w) / 1000)
        if bbox_thick < 1: bbox_thick = 1
        fontScale = 0.75 * bbox_thick
        (x1, y1), (x2, y2) = (coor[0], coor[1]), (coor[2], coor[3])
        cv2.rectangle(image, (x1, y1), (x2, y2), bbox_color, bbox_thick * 2)
        label = f"{self._classes[class_ind]} {score:.2f}" if show_confidence else f"{self._classes[class_ind]}"
        (text_width, text_height), baseline = cv2.getTextSize(label, cv2.FONT_HERSHEY_COMPLEX_SMALL,
                                                              fontScale, thickness=bbox_thick)
        cv2.rectangle(image, (x1, y1), (x1 + text_width, y1 - text_height - baseline), bbox_color,
                      thickness=cv2.FILLED)
        cv2.putText(image, label, (x1, y1 - 4), cv2.FONT_HERSHEY_COMPLEX_SMALL,
                    fontScale, (255, 255, 0), bbox_thick, lineType=cv2.LINE_AA)
        cv2.imwrite(self._out_image_path, image)

